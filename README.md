<!--
 * @Descripttion: 
 * @Author: junjiaHuang
 * @Date: 2020-08-26 17:50:22
 * @LastEditors: junjiaHuang
 * @LastEditTime: 2020-08-28 10:04:06
-->
# vue3-demo

- [x] 基本路由
  - [ ] 路由守卫
    - [ ] 全局路由守卫
    - [ ] 页面路由守卫
    - [ ] 组件路由守卫
  - [ ] 子路由嵌套
  - [ ] 滚动
  - [ ] 传参
    - [ ] 两种参数
- [x] keep-alive
- [x] 助手函数
- [x] 依赖注入
- [x] setUp
  - [ ] ref
  - [ ] useXxx
  - [ ] 其他

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
