/*
 * @Descripttion: 
 * @Author: junjiaHuang
 * @Date: 2020-08-26 17:50:14
 * @LastEditors: junjiaHuang
 * @LastEditTime: 2020-08-28 09:48:07
 */
import { createStore } from 'vuex'

export default createStore({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
