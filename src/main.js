/*
 * @Descripttion: 
 * @Author: junjiaHuang
 * @Date: 2020-08-26 17:50:14
 * @LastEditors: junjiaHuang
 * @LastEditTime: 2020-08-28 09:51:17
 */
import * as Vue from 'vue'
import App from './App.vue'
// import {router, routerHistory} from './router'
import router from './router'
import store from './store'

let app = Vue.createApp(App)
console.log(app)

// 全局注入
app.provide('test', function(){
  console.log('我是注入')
})

// 全局助手函数
app.config.globalProperties.$getTest = () => {
  console.log('我是助手函数')
}


app.use(store)
  .use(router)
  .mount('#app')

