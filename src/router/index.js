/*
 * @Descripttion: 
 * @Author: junjiaHuang
 * @Date: 2020-08-26 17:50:14
 * @LastEditors: junjiaHuang
 * @LastEditTime: 2020-08-28 09:49:20
 */

// 两种模式，hash和history
import { createRouter, createWebHistory } from 'vue-router' 
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import News from '../views/News.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home, 
  },
  {
    path: '/about',
    name: 'About',
    component: About, 
  },
  {
    path: '/news',
    name: 'News',
    component: News,
  },
]
// export 
const routerHistory = createWebHistory()
// export
const router = createRouter({
  history: routerHistory,
  strict: true,
  routes,
})

router.beforeEach((to, from, next) => {
  console.warn('beforeEach')
  // console.log(to)
  // console.log(to.name)
  // console.log(from)
  // console.log(from.name)
  next()
})
export default router

// 目前路由不存在该钩子
// router.beforeRouterEnter((to, from, next) => {
//   console.log('beforeRouterEnter')
//   console.log(to)
//   console.log(from)
//   next()
// })
